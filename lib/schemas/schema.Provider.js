'use strict';

const mongoose = require('bluebird').promisifyAll(require('mongoose'));
const Schema = mongoose.Schema;

const ProviderSchema = mongoose.model('Provider', {
    driver: String,
    token: String,
    refreshToken: String,
    expire: Date,
    created: {type: Date, default: new Date()},
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    lastUpdate: {type: Date, default: new Date()}
});

module.exports = ProviderSchema;
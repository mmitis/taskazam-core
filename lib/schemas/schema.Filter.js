'use strict';

const HashGenerator = require('./../utils/HashGenerator');
const findOrCreate = require('mongoose-findorcreate');
const mongoose = require('bluebird').promisifyAll(require('mongoose'));
const Schema = mongoose.Schema;

const FilterSchema = new Schema({
    name: {default  : 'Unknown Filter' , type : String},
    params: {
        byTags: {default: [], type: Array},
        byName: {default: null, type: String},
        byDate: {
            dateFrom : { default : null, type: Date},
            dateTo : { default : null, type: Date}
        }
    },
    user: {type: Schema.Types.ObjectId, ref: 'User'}

});

FilterSchema.plugin(findOrCreate);
module.exports = mongoose.model('Filter', FilterSchema);
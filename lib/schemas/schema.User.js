'use strict';

const HashGenerator = require('./../utils/HashGenerator');
const findOrCreate = require('mongoose-findorcreate');
const mongoose = require('bluebird').promisifyAll(require('mongoose'));
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: String,
    lang: String,
    accessToken: {type: Object, default: HashGenerator.getDatedToken()},
    providers: [{type: Schema.Types.ObjectId, ref: 'Provider'}],
    authId: String,
    syncTime: {type: Number, default: 1}
});

UserSchema.plugin(findOrCreate);
module.exports = mongoose.model('User', UserSchema);
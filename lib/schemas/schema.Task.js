'use strict';

const HashGenerator = require('./../utils/HashGenerator');
const findOrCreate = require('mongoose-findorcreate');
const mongoose = require('bluebird').promisifyAll(require('mongoose'));
const Schema = mongoose.Schema;

const TaskSchema = new Schema({
    _id: {type: String, index: false},
    name: {type: String, index: true},
    provider: String,
    url: String,
    uniqueProviderId: String,
    description: String,
    baseTime: Date,
    updateTime: Date,

    saveTime: {type: Date, default: Date},
    user: {type: Schema.Types.ObjectId, ref: 'User'}
});
TaskSchema.on('index', function (error) {
    // "_id index cannot be sparse"
    console.log(error);
});
TaskSchema.plugin(findOrCreate);
module.exports = mongoose.model('Task', TaskSchema);
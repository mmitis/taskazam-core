'use strict';
const google = require('googleapis'),
    googleAuth = require('google-auth-library'),
    DataProvider = require('./../abstracts/DataProvider'),
    Task = require('./../abstracts/TaskModel'),
    moment = require('moment'),
    Errors = require('./../utils/ErrorHandling');

/**
 *  Class of the Google Events Provider
 */
class GoogleCalendarProvider extends DataProvider {

    /**
     * Creates Google Calendar Provider
     * @param _redirectUri {string} url where should redirect after log in
     * @param _providerConfig {string} configuration data for the provider
     * @param _cacheManager {CacheManager} object of cache
     * @param _authorizer {function} function which authorizes user in db
     */
    constructor(_redirectUri, _providerConfig, _cacheManager, _authorizer) {
        super(_redirectUri, _providerConfig, _cacheManager, _authorizer);
        this.provider_name = 'google';
        this.scopes = ['https://www.googleapis.com/auth/calendar.readonly', 'https://www.googleapis.com/auth/userinfo.profile'];
        this.auth = new googleAuth();
    }

    /**
     * Gets instance of the data client
     * @returns {OAuth client}
     */
    getClient() {
        return new this.auth.OAuth2(this.providerConfig.client_id, this.providerConfig.client_secret, this.redirectUri);
    }

    /**
     * Get the authorization token for the selected data
     * @param userId {string} id of the user
     * @param code {string} set as option - authorization code
     * @returns {Promise}
     */
    setAuthorization(userId, {code}) {
        //Validate params
        if (!(code && code.length > 10)) {
            return Promise.reject(Errors.INVALID_PARAMS());
        }
        return new Promise((resolve, reject)=> {
            try {
                this.oauth2Client = this.getClient();
                this.oauth2Client.getToken(code, (err, token)=> {
                    if (err) {
                        reject(Errors.CANNOT_PARSE(err));
                        return;
                    }
                    resolve({
                        token: token.access_token,
                        refresh_token: token.refresh_token,
                        expire: token.expiry_date
                    });
                });
            } catch (err) {
                console.log(err);
                reject(Errors.CANNOT_CONNECT(err));
            }

        });
    }

    /**
     * Creates URL where user can authorize the service
     * @param userId {string} user id code
     * @returns {Promise.<string>}
     */
    getAuthorizeUrl(userId, state) {
        this.oauth2Client = this.getClient();
        return new Promise((resolve)=> {
            resolve(this.oauth2Client.generateAuthUrl({
                access_type: 'offline',
                scope: this.scopes,
                redirect_uri: this.redirectUri,
                approval_prompt: 'force',
                state: state,
            }));
        });
    }

    /**
     * Gets all data items  from the provider
     * @param userId
     * @returns {Promise}
     */
    getItems(userId) {
        return new Promise((resolve, reject)=> {
            this.isAuthorized(userId)
                .then((providerData)=> {
                    if (!providerData) {
                        reject(Errors.INVALID_TOKEN());
                        return;
                    }
                    const token = providerData.token;
                    const refresh_token = providerData.refresh_token;
                    console.info('[GET_ITEMS] Data taken with tokens : %s %s', token, refresh_token);
                    try {
                        this.oauth2Client = this.getClient();
                        this.oauth2Client.setCredentials({
                            access_token: token,
                            refresh_token
                        });
                        let client = this.oauth2Client;
                        const calendar = google.calendar('v3');
                        calendar.events.list({
                            auth: client,
                            calendarId: 'primary',
                            timeMin: (new Date()).toISOString(),
                            maxResults: 50,
                            singleEvents: false
                        }, (err, response)=> {
                            if (err) {
                                reject(Errors.EXTERNAL_ERROR(err));
                                return;
                            }
                            const events = response.items;
                            resolve(events);
                        }, (err)=> {
                            reject(Errors.INVALID_QUERY(err));
                        });
                    } catch (err) {
                        reject(Errors.CANNOT_CONNECT(err));
                    }
                });
        });
    }

    /**
     * Transformation function to Task format
     * @param original {object} original google event object
     * @returns {TaskModel}
     */
    singleItemTransform(original) {
        console.info('[GET_ITEMS] Transform to task : %s', JSON.stringify(original));
        return new Task({
            name: original.summary,
            provider: 'google',
            url: original.htmlLink,
            uniqueProviderId: original.id,
            description: original.desc,
            baseTime: moment(original.start).unix(),
            updateTime: moment(original.updated).unix()
        });
    }
}

module.exports = GoogleCalendarProvider;
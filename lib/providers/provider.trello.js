'use strict';
const OAuth = require('oauth'),
    Trello = require('node-trello'),
    DataProvider = require('./../abstracts/DataProvider'),
    Task = require('./../abstracts/TaskModel'),
    moment = require('moment'),
    Errors = require('./../utils/ErrorHandling');

/**
 *  Class of the Trello Provider
 */
class TrelloProvider extends DataProvider {

    /**
     * Creates Trello Provider
     * @param {string} _redirectUri  url where should redirect after log in
     * @param {string} _providerConfig  configuration data for the provider
     * @param {CacheManager} _cacheManager  object of cache
     * @param {function} _authorizer  function which authorizes user in db
     */
    constructor(_redirectUri, _providerConfig, _cacheManager, _authorizer) {
        super(_redirectUri, _providerConfig, _cacheManager, _authorizer);
        this.provider_name = 'trello';
        this.oauth = new OAuth.OAuth(
            'https://trello.com/1/OAuthGetRequestToken',
            'https://trello.com/1/OAuthGetAccessToken',
            this.providerConfig.key,
            this.providerConfig.secret,
            '1.0A',
            this.redirectUri,
            'HMAC-SHA1'
        );
    }

    /**
     * Gets instance of the data client
     * @returns {Oauth.Client}
     */
    getClient() {
        return this.oauth;
    }

    /**
     * Get the authorization token for the selected data
     * @param {string} userId  id of the user
     * @param {string} oauth_token  set as option - authorization token
     * @param {string} oauth_verifier  set as option - authorization verifier code
     * @returns {Promise}
     */
    setAuthorization(userId, {oauth_token, oauth_verifier}) {
        //Validate params
        if (!(oauth_token && oauth_token.length > 10 && oauth_verifier && oauth_verifier.length > 10)) {
            return Promise.reject(Errors.INVALID_PARAMS());
        }
        return new Promise((resolve, reject)=> {
            try {
                this.cacheManager.getKey(this.provider_name + '_' + userId + '_tmp')
                    .then((secretData)=> {
                        try {
                            let data = JSON.parse(secretData);
                            this.oauth.getOAuthAccessToken(data.token, data.secret, oauth_verifier, (err, oauthAccessToken)=> {
                                if (err !== null) {
                                    reject(Errors.EXTERNAL_ERROR(err));
                                } else {
                                    resolve({
                                        token: oauthAccessToken,
                                        expire: 0
                                    });
                                }
                            });
                        } catch (err) {
                            reject(Errors.CANNOT_PARSE(err));
                        }
                    });
            } catch (err) {
                reject(Errors.CANNOT_CONNECT(err));
            }
        });
    }

    /**
     * Creates URL where user can authorize the service
     * @param {string} userId  user id code
     * @returns {Promise.<string>}
     */
    getAuthorizeUrl(userId, state) {
        return new Promise((resolve, reject)=> {
            this.getClient().getOAuthRequestToken((err, oauthToken, oauthTokenSecret)=> {
                if (err) {
                    reject(Errors.CANNOT_CONNECT(err));
                    return;
                }
                this.storeTemporaryToken(userId, JSON.stringify({token: oauthToken, secret: oauthTokenSecret}));
                resolve('https://trello.com/1/OAuthAuthorizeToken?name=Taskazam&oauth_token=' + oauthToken + '&state=' + state);
            });
        });
    }

    /**
     * Gets all data items  from the provider
     * @param userId
     * @returns {Promise}
     */
    getItems(userId) {
        return new Promise((resolve, reject)=> {
            this.isAuthorized(userId)
                .then((providerData)=> {
                    if (!providerData) {
                        reject(Errors.INVALID_TOKEN());
                        return;
                    }
                    const token = providerData.token;
                    try {
                        const trelloInstance = new Trello(this.providerConfig.key, token);
                        trelloInstance.get("/1/members/me/cards", (err, data)=> {
                            if (err) {
                                reject(Errors.EXTERNAL_ERROR(err));
                                return;
                            }
                            resolve(data);
                        });
                    } catch (err) {
                        reject(Errors.CANNOT_CONNECT(err));
                    }
                });
        });
    }

    /**
     * Transformation function to Task format
     * @param {object} original  original trello event object
     * @returns {TaskModel}
     */
    singleItemTransform(original) {
        return new Task({
            name: original.name,
            provider: 'trello',
            url: original.shortUrl,
            uniqueProviderId: original.id,
            description: original.desc,
            baseTime: moment(original.dateLastActivity).unix(),
            updateTime: moment(original.dateLastActivity).unix()
        });
    }
}

module.exports = TrelloProvider;
'use strict';
const DataProvider = require('./../abstracts/DataProvider'),
    FB = require('fb'),
    request = require('request'),
    Task = require('./../abstracts/TaskModel'),
    moment = require('moment'),
    Errors = require('./../utils/ErrorHandling');

/**
 *  Class of the Facebook Events Provider
 */
class FacebookProvider extends DataProvider {

    /**
     * Creates Facebook Provider
     * @param _redirectUri {string} url where should redirect after log in
     * @param _providerConfig {string} configuration data for the provider
     * @param _cacheManager {CacheManager} object of cache
     * @param _authorizer {function} function which authorizes user in db
     */
    constructor(_redirectUri, _providerConfig, _cacheManager, _authorizer) {
        super(_redirectUri, _providerConfig, _cacheManager, _authorizer);
        this.provider_name = 'facebook';
        this.authorizationURL = 'https://www.facebook.com/dialog/oauth';
    }

    /**
     * Get the authorization token for the selected data
     * @param userId {string} id of the user
     * @param code {string} set as option - authorization code
     * @returns {Promise}
     */
    setAuthorization(userId, {code}) {
        //Validate params
        if (!(code && code.length > 10)) {
            return Promise.reject(Errors.INVALID_PARAMS());
        }
        return new Promise((resolve, reject)=> {
            try {
                const confifmationUrl = "https://graph.facebook.com/oauth/access_token?" +
                    "redirect_uri=" + encodeURIComponent(this.redirectUri) +
                    "&client_id=" + this.providerConfig.client_id +
                    "&client_secret=" + this.providerConfig.client_secret +
                    "&code=" + code;
                let requestOptions = {
                    method: "POST",
                    uri: confifmationUrl
                };
                request(requestOptions,
                    (err, response, body)=> {
                        if (err !== null) {
                            reject(Errors.CANNOT_PARSE(err));
                        }
                        if (response && response.statusCode === 200 &&
                            response.headers && /.*text\/plain.*/.test(response.headers['content-type'])) {
                            const oauthAccessToken = body.replace('access_token=', '').replace('expires=', '').split('&');
                            resolve({
                                token: oauthAccessToken[0],
                                expire: oauthAccessToken[1]
                            });

                        } else {
                            reject(Errors.EXTERNAL_ERROR(JSON.parse(body).error), 7);
                        }
                    });
            } catch (err) {
                reject(Errors.CANNOT_CONNECT());
            }
        });
    }

    /**
     * Creates URL where user can authorize the service
     * @returns {Promise.<string>}
     */
    getAuthorizeUrl(userId, state) {
        return Promise.resolve(this.authorizationURL + '?app_id=' + this.providerConfig.client_id + '&redirect_uri=' + encodeURIComponent(this.redirectUri)+'&state=' + state);
    }

    /**
     * Gets all data items  from the provider
     * @param userId
     * @returns {Promise}
     */
    getItems(userId) {
        return new Promise((resolve, reject)=> {
            this.isAuthorized(userId)
                .then((providerData)=> {
                    if (!providerData) {
                        reject(Errors.INVALID_TOKEN());
                        return;
                    }
                    const token = providerData.token;
                    try {
                        FB.api('/me/events', {
                            fields: ['name', 'id', 'description', 'updated_time', 'cover', 'place', 'start_time'],
                            limit: 100,
                            access_token: token
                        }, (data)=> {
                            console.log(data);
                            resolve(data.data);
                        });

                    } catch (err) {
                        reject(Errors.INVALID_QUERY(err));
                    }
                });
        });
    }

    /**
     * Transformation function to Task format
     * @param original {object} original facebook event object
     * @returns {TaskModel}
     */
    singleItemTransform(original) {
        return new Task({
            name: original.name,
            provider: 'facebook',
            url: 'http://facebook.com/events/' + original.id,
            uniqueProviderId: original.id,
            description: original.description,
            baseTime: moment(original.start_time).unix(),
            updateTime: moment(original.update_time).unix(),
            image: original.cover ? original.cover.source : null
        });
    }
}

module.exports = FacebookProvider;
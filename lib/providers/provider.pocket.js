'use strict';
const pocket = require('pocket-api'),
    DataProvider = require('./../abstracts/DataProvider'),
    Task = require('./../abstracts/TaskModel'),
    request = require('request'),
    Errors = require('./../utils/ErrorHandling');

/**
 *  Class of the Pocket Provider
 */
class PocketProvider extends DataProvider {

    /**
     * Creates Pocket Provider
     * @param _redirectUri {string} url where should redirect after log in
     * @param _providerConfig {string} configuration data for the provider
     * @param _cacheManager {CacheManager} object of cache
     * @param _authorizer {function} function which authorizes user in db
     */
    constructor(_redirectUri, _providerConfig, _cacheManager, _authorizer) {
        super(_redirectUri, _providerConfig, _cacheManager, _authorizer);
        this.provider_name = 'pocket';
    }

    /**
     * Get the authorization token for the selected data
     * @param userId {string} id of the user
     * @returns {Promise}
     */
    setAuthorization(userId) {
        return new Promise((resolve, reject)=> {
            try {
                this.getTemporaryToken(userId)
                    .then((tempToken)=> {
                        tempToken = JSON.parse(tempToken);
                        let options = {
                            url: 'https://getpocket.com/v3/oauth/authorize',
                            method: 'POST',
                            form: {
                                code: tempToken.code,
                                consumer_key: this.providerConfig.client_id
                            },
                            headers: {
                                'X-Accept': 'application/json'
                            }
                        };
                        request(options, (err, response, body)=> {
                            try {
                                let data = JSON.parse(body);
                                resolve({
                                    token: data.access_token,
                                    expire: 0
                                });
                            } catch (err) {
                                reject(Errors.CANNOT_PARSE(err));
                            }
                        });
                    });
            } catch (err) {
                reject(Errors.CANNOT_CONNECT(err));
            }

        });
    }

    /**
     * Creates URL where user can authorize the service
     * @param userId {string} user id code
     * @param {string} state - additional state
     * @returns {Promise.<string>}
     */
    getAuthorizeUrl(userId, state) {
        return new Promise((resolve, reject)=> {
            let options = {
                url: 'https://getpocket.com/v3/oauth/request',
                method: 'POST',
                form: {
                    redirect_uri: this.redirectUri,
                    consumer_key: this.providerConfig.client_id,
                    state: state
                },
                headers: {
                    'X-Accept': 'application/json'
                }
            };
            request(options, (err, response, body)=> {
                if (err !== null) {
                    reject(Errors.INVALID_QUERY(err));
                    return;
                }
                if (response && response.statusCode === 200) {
                    try {
                        let data = JSON.parse(body);
                        this.storeTemporaryToken(userId, JSON.stringify({code: data.code}));
                        resolve('https://getpocket.com/auth/authorize?request_token=' + data.code + '&redirect_uri=' + encodeURIComponent(this.redirectUri) + "&state=" + state);
                    } catch (err) {
                        reject(Errors.CANNOT_PARSE(err));
                    }
                } else {
                    reject(Errors.CANNOT_PARSE(err));
                }
            });
        });
    }

    /**
     * Gets all data items  from the provider
     * @param userId
     * @returns {Promise}
     */
    getItems(userId) {
        return new Promise((resolve, reject)=> {
            this.isAuthorized(userId)
                .then((providerData)=> {
                    if (!providerData) {
                        reject(Errors.INVALID_TOKEN());
                        return;
                    }
                    const token = providerData.token;
                    try {
                        pocket.getArticles(this.providerConfig.client_id, token, (error, data)=> {
                            let resultList = [];
                            if (data && data.list) {
                                for (let item in data.list) {
                                    if (data.list.hasOwnProperty(item)) {
                                        resultList.push(data.list[item]);
                                    }
                                }
                            }
                            resolve(resultList);
                        });
                    } catch (err) {
                        reject(Errors.EXTERNAL_ERROR(err));
                    }
                });
        });
    }

    /**
     * Transformation function to Task format
     * @param original {object} original pocket event object
     * @returns {TaskModel}
     */
    singleItemTransform(original) {
        return new Task({
            name: original.given_title,
            provider: this.provider_name,
            url: original.given_url,
            description: original.excerpt,
            uniqueProviderId: original.item_id,
            baseTime: parseInt(original.time_added),
            updateTime: parseInt(original.time_updated)
        });
    }
}

module.exports = PocketProvider;
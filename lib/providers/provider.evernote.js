'use strict';
const Evernote = require('evernote').Evernote,
    DataProvider = require('./../abstracts/DataProvider'),
    Task = require('./../abstracts/TaskModel'),
    moment = require('moment'),
    Errors = require('./../utils/ErrorHandling');
/**
 *  Class of the Evernote tasks Provider
 */
class EvernoteProvider extends DataProvider {

    /**
     * Creates Evernote Provider
     * @param _redirectUri {string} url where should redirect after log in
     * @param _providerConfig {string} configuration data for the provider
     * @param _cacheManager {CacheManager} object of cache
     * @param _authorizer {function} function which authorizes user in db
     */
    constructor(_redirectUri, _providerConfig, _cacheManager, _authorizer) {
        super(_redirectUri, _providerConfig, _cacheManager, _authorizer);
        this.provider_name = 'evernote';
    }

    /**
     * Gets instance of the data client
     * @returns {Evernote.Client}
     */
    getClient() {
        return new Evernote.Client({
            consumerKey: this.providerConfig.client_id,
            consumerSecret: this.providerConfig.client_secret,
            sandbox: [true]
        });
    }

    /**
     * Get the authorization token for the selected data
     * @param userId {string} id of the user
     * @param oauth_token {string} set as option - authorization token
     * @param oauth_verifier {string} set as option - authorization verifier code
     * @returns {Promise}
     */
    setAuthorization(userId, {oauth_token, oauth_verifier}) {
        //Validate params
        if (!(oauth_token && oauth_token.length > 10 && oauth_verifier && oauth_verifier.length > 10)) {
            return Promise.reject(Errors.INVALID_PARAMS());
        }

        return new Promise((resolve, reject) => {
            try {
                let client = this.getClient();
                this.cacheManager.getKey(this.provider_name + '_' + userId + '_tmp')
                    .then((secretData) => {
                        const data = JSON.parse(secretData);
                        client.getAccessToken(data.token, data.secret, oauth_verifier, (err, oauthAccessToken) => {
                            if (err !== null) {
                                reject(Errors.EXTERNAL_ERROR(err));
                            } else {
                                resolve({
                                    token: oauthAccessToken,
                                    expire: 0
                                });
                            }
                        });
                    });
            } catch (err) {
                reject(Errors.CANNOT_CONNECT(err));
            }
        });
    }

    /**
     * Creates URL where user can authorize the service
     * @param userId {string} user id code
     * @returns {Promise.<string>}
     */
    getAuthorizeUrl(userId, state) {
        var that = this;
        return new Promise((resolve) => {
            let client = this.getClient();
            client.getRequestToken(that.redirectUri, (error, oauthToken, oauthTokenSecret) => {
                that.storeTemporaryToken(userId, JSON.stringify({token: oauthToken, secret: oauthTokenSecret}));
                resolve(client.getAuthorizeUrl(oauthToken) + '&state=' + state);
            });
        });
    }

    /**
     * Gets all data items  from the provider
     * @param userId
     * @returns {Promise}
     */
    getItems(userId) {
        return new Promise((resolve, reject) => {
            this.isAuthorized(userId)
                .then((providerData) => {
                    if (!providerData) {
                        reject(Errors.INVALID_TOKEN());
                        return;
                    }
                    const token = providerData.token;
                    try {
                        const client = new Evernote.Client({token: token});
                        const noteStore = client.getNoteStore();
                        noteStore.listNotebooks(() => {
                            let filter = new Evernote.NoteFilter();
                            let resultSpec = new Evernote.NotesMetadataResultSpec();
                            resultSpec.includeTitle = true;
                            resultSpec.includeTags = true;
                            resultSpec.includeTagGuids = true;
                            resultSpec.includeAttributes = true;

                            resultSpec.includeUpdated = true;
                            noteStore.findNotesMetadata(token, filter, 0, 100, resultSpec, (err, notesMeta) => {
                                if (err) {
                                    reject(Errors.EXTERNAL_ERROR(err));
                                }
                                else {
                                    resolve(notesMeta.notes);
                                }
                            });
                        });
                    } catch (err) {
                        reject(Errors.INVALID_QUERY(err));
                    }
                });
        });
    }

    /**
     * Transformation function to Task format
     * @param original {object} original facebook event object
     * @returns {TaskModel}
     */
    singleItemTransform(original) {
        return new Task({
            name: original.title,
            provider: 'evernote',
            url: null,
            uniqueProviderId: original.guid,
            description: '',
            aseTime: original.updated,
            updateTime: moment(original.updated).unix()
        });
    }
}

module.exports = EvernoteProvider;
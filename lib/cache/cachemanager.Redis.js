/**
 * Created by marekmitis on 2016-04-25.
 */
'use strict';

var bluebird = require('bluebird');
var redis = require('redis');
var url = require('url');
var CacheManager = require('../abstracts/CacheManager');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

class RedisCache extends CacheManager {

    constructor(redisConfig) {
        super();
        var redisURL = url.parse(redisConfig);
        this.client = redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});
        this.client.auth(redisURL.auth.split(":")[1]);
        this.client.on("error", function (err) {
            console.log("Error " + err);
        });
    }

    getClient() {
        return this.client;
    }

    getKey(key) {
        return this.client.getAsync(key).then((value)=> {
            return JSON.parse(value);
        });
    }

    setKey(key, value) {
        return this.client.set(key, JSON.stringify(value));
    }
}

module.exports = RedisCache;
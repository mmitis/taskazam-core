const UserManager = require('./db/usermanager.Mongo'),
    ContentManager = require('./db/contentmanager.Mongo'),
    CacheManager = require('./cache/cachemanager.Redis'),
    ProviderConfig = require('./configs/config.providers'),
    Errors = require('./utils/ErrorHandling');

const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

/*
 TODO: - validating params, full comments, tests, tokenBy class, filtering providers, long-term tokens, clean-up
 */
class TaskazamClient {

    constructor({
        dbUrl,
        cacheUrl,
        backUrl
    }) {
        this.cacheManager = new CacheManager(cacheUrl);
        this.providerList = this._getProviders(ProviderConfig, this.cacheManager, backUrl);

        mongoose.connect(dbUrl);
        mongoose.connection.on('error', function (err) {
            console.error('MongoDB MongoContentManager connection error: ' + err);
            process.exit(-1);
        });
        mongoose.connection.on('connected', function () {
            console.log('Mongoose MongoContentManager default connection open to ' + dbUrl);
        });

        process.on('SIGINT', function () {
            mongoose.connection.close(function () {
                process.exit(0);
            });
        });

        this.db = {
            users: new UserManager(),
            content: new ContentManager()
        };

    }

    /**
     * Gets all user ids list
     * @returns {*}
     */
    getAllUsersIds() {
        return this.db.users.getAllUsersIds();
    }

    /**
     * Gets user profile by selected token
     * @param userId {string} active token of the user
     * @returns {*}
     */
    getUserProfile(userId) {
        return this.db.users.getUser(userId)
            .then((profile)=> {
                profile = this._decorateProviders(profile.toObject());
                return profile;
            });
    }


    _decorateProviders(profile) {
        profile.providersList = [];
        for (let providerName in this.providerList) {
            if (this.providerList.hasOwnProperty(providerName)) {
                let provider = this.providerList[providerName];
                profile.providersList.push({
                    name: provider.provider_name,
                    token: this._searchForProvider(provider.provider_name, profile.providers)
                });
            }
        }
        delete profile.providers;
        return profile;
    }

    _searchForProvider(name, providerArray) {
        for (let providerName in providerArray) {
            if (providerArray.hasOwnProperty(providerName)) {
                let provider = providerArray[providerName];
                if (provider.driver == name) {
                    return {
                        token: provider.token,
                        expire: provider.expire
                    };
                }
            }
        }
        return null;
    }

    /**
     * Authorizes user in the provider
     * @param {string} userId  user id
     * @param {string} providerName  name of the provider
     * @param {object} requestData  additional data needed to authorize
     * @returns {Promise.<T>}
     */
    setUserAuthorize(userId, providerName, requestData) {
        return this._getProvider(providerName)
            .setAuthorization(userId, requestData)
            .then((tokenData)=> {
                this.db.users.updateAuthProvider(userId, providerName, tokenData.token, tokenData.refresh_token || null, tokenData.expire);
                return tokenData;
            });
    }

    /**
     * Force generated link to the authorization service
     * @param userId {string} user id
     * @param providerName {string} name of the provider
     * @param {string} state - additional state
     * @returns {Promise.<string>} url to the authorization
     */
    getForceAuthorizeUrl(userId, providerName, state = "") {
        return this._getProvider(providerName)
            .getAuthorizeUrl(userId, state);
    }

    /**
     * Gets information about single provider status
     * @param userId {string} user id
     * @param providerName {string} name of the provider
     * @param {string} additional state
     * @returns {Promise.<T>}
     */
    getAuthProvider(userId, providerName, state = "") {
        return this._getProvider(providerName)
            .isAuthorized(userId)
            .then((token)=> {
                return this.providerList[providerName]
                    .getAuthorizeUrl(userId, state)
                    .then((url)=> {
                        return {
                            token: token,
                            url: url
                        }
                    });
            });
    }

    /**
     * Creates new filter
     *
     */

    createFilter(userId, name, params) {
        return this.db.content.createFilter(userId, name, params);
    }

    getAvailableFilters(userId) {
        return this.db.content.getAllUserFilters(userId);
    }

    removeProviderAuth(userId, providerName) {
        let provider = this._getProvider(providerName);
        return provider
            .isAuthorized(userId)
            .then((isAuthorized)=> {
                if (isAuthorized) {
                    return this.db.users.removeAuthProvider(userId, providerName);
                }
            });

    }

    getItemsFromStorage(userId, filters, groups) {
        if (!filters) {
            filters = [];
        }
        return this.db.content.getSomeUserFilters(userId, filters)
            .then((filtersArr) => {

                const schema = this._getFilterSchema(filtersArr);
                console.info('Searched with schema %j', schema);

                return this.db.content.getItems(userId, schema).then((items)=> {
                    if (!groups) {
                        return [{
                            name: 'All',
                            items: items
                        }];
                    }
                    else {
                        return this._groupingMagic(userId, items, groups);
                    }
                })
            });
    }

    _groupingMagic(userId, items, groups) {
        return this.db.content.getSomeUserFilters(userId, groups).then((groupsArr) => {
            items.forEach((item) => {
                groupsArr.forEach((group)=> {
                    group.items = [];
                    if (group.params.byName && item.name.match(new RegExp(group.params.byName, "i"))) {
                        group.items.push(item.toObject());
                    }
                });
            });

            return groupsArr.map((group)=> {
                console.log(group);
                return {
                    name: group.name,
                    items: group.items
                };
            })
        });
    }

    _getFilterSchema(filtersArr) {
        var schema = {};
        filtersArr.map((filter) => {
            //Search by name
            if (filter.params.byName) {
                schema.name = new RegExp(filter.params.byName, "i");
            }
            if (filter.params.byDescription) {
                schema.description = new RegExp(filter.params.byDescription, "i");
            }
        });
        return schema;
    }

    /**
     * Returns all items for the user
     * @param userId
     * @returns {Promise.<T>}
     * @private
     */
    _getAllItems(userId) {
        const promiseMap = [];
        for (let providerName in this.providerList) {
            if (this.providerList.hasOwnProperty(providerName)) {
                var provider = this.providerList[providerName];
                promiseMap.push(this._singleProviderItems.call(this, userId, provider.provider_name));
            }
        }
        return Promise.all(promiseMap)
            .then((allRequests)=> {
                let mergedResponse = [];
                allRequests.forEach((response) => {
                    if (Array.isArray(response)) {
                        mergedResponse = [...mergedResponse, ...response];
                    }
                });
                return this.db.content.storeAllItems(userId, mergedResponse)
                    .then((items)=> {
                        return items;
                    });
            })
            .catch((err) => {
                console.log(err);
            })
    }

    _singleProviderItems(userId, providerName) {
        let provider = this._getProvider(providerName);
        return provider
            .isAuthorized(userId)
            .then((isAuthorized)=> {
                if (isAuthorized) {
                    return provider.getCachedItems(userId)
                        .then(provider.promisedTransformItems.bind(provider));
                } else {
                    Promise.resolve([]);
                }
            });
    }

    /**
     * Function which chekcs if user is logged into selected provider
     * @param userId {string} user id
     * @param providerName {string} name of the provider
     * @returns {Promise.<T>}
     * @private
     */
    _authorize(userId, providerName) {
        const authFunc = this.db.users.getProviderByUserAndDriver;
        return authFunc(userId, providerName)
            .then((provider) => provider ? provider : null);
    }

    /**
     * Loads information about available providers
     * @param providerConfig {object}
     * @param cache {CacheManager}
     * @param backUrl {string}
     * @returns {{}}
     * @private
     */
    _getProviders(providerConfig, cache, backUrl) {
        let providerList = {};
        for (let providerIndex in providerConfig) {
            if (providerConfig.hasOwnProperty(providerIndex)) {
                let provider = providerConfig[providerIndex];
                providerList[providerIndex] = new provider.driver(backUrl + providerIndex, provider.secrets, cache, this._authorize.bind(this));
            }
        }
        return providerList;
    }

    _getProvider(providerName) {
        if (this.providerList.hasOwnProperty(providerName)) {
            return this.providerList[providerName];
        } else {
            return Promise.reject(Errors.PROVIDER_NOT_FOUND());
        }
    }

}

module.exports = {
    TaskazamClient,
    configs: require('./configs/config.providers'),
    schemas: {
        User: require('./schemas/schema.User')
    }
};
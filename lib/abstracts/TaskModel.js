'use strict';
var md5 = require('md5');

class TaskModel {
    constructor({
        name,
        provider,
        original,
        description,
        uniqueProviderId,
        url,
        image,
        updateTime,
        baseTime
    }) {
        this.name = name;
        this.provider = provider;
        this.original = original;
        this.provider = {
            name: provider,
            hash: md5(provider).substr(0, 5),
            itemId: uniqueProviderId
        };
        this.description = description;
        this.url = url;
        this.time = {
            update: updateTime,
            base: baseTime
        };
        this.image = image;
    }


    getDbObject() {
        return {
            _id: this.provider.itemId,
            name: this.name,
            provider: this.provider.name,
            url: this.url,
            uniqueProviderId: this.provider.itemId,
            description: this.description,
            baseTime: this.time.base,
            updateTime: this.time.update,
            saveTime: new Date().getTime()
        }
    }
}

module.exports = TaskModel;
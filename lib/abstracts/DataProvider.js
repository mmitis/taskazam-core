/**
 * Created by marekmitis on 2016-05-03.
 */
'use strict';

const Task = require('./TaskModel');
const CACHE_DURATION = 20000;

class DataProvider {
    /**
     * Constructor for the provider
     * @param _redirectUri url where authorization hould be redirected
     * @param _providerConfig needed configurations
     * @param _cacheManager cacheprovider
     */
    constructor(_redirectUri, _providerConfig, _cacheManager, _authorizer) {
        this.provider_name = 'UnknownProvider';
        this.redirectUri = _redirectUri;
        this.cacheManager = _cacheManager;
        this.providerConfig = _providerConfig;
        this.authorizer = _authorizer;
    }

    /**
     * Gets authorization client
     * @returns {null}
     */
    getClient() {
        return null;
    }

    /**
     * Resolves if the user is authorized
     * @param userId - user to authorize
     */
    isAuthorized(userId) {
        return this.authorizer(userId, this.provider_name);
    }

    setAuthorization(userId, req) {
        return Promise.resolve(null);
    };

    /**
     * Creates authorization url
     * @param {string} userId - id of the logged user
     * @param {string} state - additional state
     * @return {Promise.<string>}
     */
    getAuthorizeUrl(userId, state) {
        return Promise.resolve("");
    }

    /**
     * Stores token for the authorization purpose
     * @param userId - id of the user to store
     * @param token - token string
     */
    storeToken(userId, token, timestamp) {
        let saveData = JSON.stringify({
            token,
            expires: timestamp
        });
        this.cacheManager.setKey(this.provider_name.toString().toLocaleLowerCase() + '_' + userId, saveData);
        return token;
    }

    getToken(userId, allFlag) {
        return new Promise((resolve)=> {
            return this.cacheManager.getKey(this.provider_name.toString().toLocaleLowerCase() + '_' + userId).then((result)=> {
                if (result !== null) {
                    try {
                        let parsedData = JSON.parse(result);
                        if (allFlag) {
                            resolve(parsedData);
                        } else {
                            resolve(parsedData.token);
                        }

                    } catch (e) {
                        resolve(e);
                    }
                } else {
                    resolve(null);
                }
            });
        });
    }

    storeTemporaryToken(userId, valueObject) {
        this.cacheManager.setKey(this.provider_name.toString().toLocaleLowerCase() + '_' + userId + "_tmp", valueObject);
        return valueObject;
    }

    getTemporaryToken(userId) {
        return this.cacheManager.getKey(this.provider_name.toString().toLocaleLowerCase() + '_' + userId + "_tmp");
    }

    currentStatus(userId) {
        return this.getToken(userId, true).then((tokenData)=> {
            return {
                provider: this.provider_name,
                tokenData
            }
        });
    }

    /**
     * Gets items by user ID
     * @param userId
     * @returns {Promise.<Array>}
     */
    getItems(userId) {
        return Promise.resolve([]);
    }

    getCachedItems(userId) {
        return new Promise((resolve)=> {
            const params = {
                limit: 100
            };
            const key = this.provider_name + 'lastItemsGetTime' + JSON.stringify(params);
            this.cacheManager.getKey(key + 'time').then((timestamp)=> {
                if (!timestamp || timestamp < new Date().getTime()) {
                    this.getItems(userId).then((data)=> {
                        this.cacheManager.setKey(key + 'data', data);
                        this.cacheManager.setKey(key + 'time', new Date().getTime() + CACHE_DURATION);
                        resolve(data);
                    });
                } else {
                    this.cacheManager.getKey(key + 'data').then((data)=> {
                        resolve(data);
                    });
                }
            })
        });
    }

    singleItemTransform(original) {
        return new Task({
            name: original.name
        })
    }


    transformItems(items) {
        if (items && items.length > 0) {
            return items.map((original)=> {
                return this.singleItemTransform(original);
            });
        } else {
            return [];
        }
    }

    promisedTransformItems(items) {
        return new Promise((resolve)=> {
            resolve(this.transformItems(items));
        })
    };
}

module.exports = DataProvider;
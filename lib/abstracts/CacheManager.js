'use strict';

class CacheManager {

    constructor(config) {
        this.db = {};
    }

    getClient() {
        return {};
    }

    getKey(key) {
        return Promise.resolve(this.db[key]);
    }

    setKey(key, value) {
        return Promise.resolve(this.db[key] = value);
    }
}

module.exports = CacheManager;
'use strict';

const errorCodes = {
    INVALID_PARAMS: (errorMsg)=>new Error('There are invalid input params.' + errorMsg, 101),
    INVALID_TOKEN: (errorMsg)=>new Error('Token does not exist or its wrong.' + errorMsg, 102),
    INVALID_QUERY: (errorMsg)=>new Error('Invalid query to provider.' + errorMsg, 103),
    CANNOT_CONNECT: (errorMsg)=>new Error('Cannot connect to the provider.' + errorMsg, 104),
    CANNOT_PARSE: (errorMsg)=>new Error('Cannot parse retrived date.' + errorMsg, 104),
    EXTERNAL_ERROR: (errorMsg)=> new Error(errorMsg, 100),

    PROVIDER_NOT_FOUND: (errorMsg)=> new Error('Provider not supported', 201)

};
module.exports = errorCodes;
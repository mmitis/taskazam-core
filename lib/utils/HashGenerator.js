const crypto = require('crypto');
const EXPIRATION_DURATION = 8720000;

class HashGenerator {

    /**
     * Generates new access token
     * @returns {{token: string, expire: number}}
     */
    static getDatedToken() {
        return {
            token: crypto.randomBytes(64).toString('hex').substr(0, 20),
            expire: new Date().getTime() + EXPIRATION_DURATION
        }
    }

    /**
     * Returns token hashed object with the new expiration date
     * @param currentToken
     * @returns {{token: string, expire: *}}
     */
    static prolongToken(currentToken) {
        return {
            token: currentToken.token,
            expire: currentToken.expire + EXPIRATION_DURATION
        }
    }

}

module.exports = HashGenerator;
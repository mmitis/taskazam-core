const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const User = require('./../schemas/schema.User');
const AuthProvider = require('./../schemas/schema.Provider');
const Filter = require('./../schemas/schema.Filter');
const HashGenerator = require('./../utils/HashGenerator');

class MongoUserManager {

    constructor() {

    }

    /**
     * Gets user by selected ID
     * @param userId
     * @returns {Promise.<T>}
     */
    getUser(userId) {
        return User.findOne({_id: userId})
            .populate('providers')
            .then(_handleEntityNotFound())
    }

    getUserIdBy(authId) {
        return User.findOne({authId: authId})
            .populate('providers')
            .then(_handleEntityNotFound())
    }

    /**
     * Get all user IDs
     */
    getAllUsersIds() {
        return User.find().lean().then((data) => {
            return data.map(item => item._id.toString());
        });
    }

    /**
     * Updates infromation about provider for selected user
     * @param {id}     userId - id of the user
     * @param {string} providerDriver - name of the driver
     * @param {string} providerRefreshToken - provider refresh token
     * @param {string} providerToken - token to save
     * @param {number} providerExpirationTime - timestamp of the token expire
     */
    updateAuthProvider(userId, providerDriver, providerToken, providerRefreshToken, providerExpirationTime) {
        this.getProviderByUserAndDriver(userId, providerDriver)
            .then((providerFound) => {
                if (providerFound !== null) {
                    providerFound.token = providerToken;
                    providerFound.refresh_token = providerRefreshToken;
                    providerFound.expire = new Date(providerExpirationTime);
                    return providerFound.saveAsync();
                } else {
                    return this._createProvider(providerDriver, providerToken, providerRefreshToken, new Date(providerExpirationTime), userId);
                }
            })
    }

    removeAuthProvider(userId, providerDriver) {
        this.getUser(userId)
            .then((userFound) => {
                if (typeof userFound.providers !== 'undefined' && Array.isArray(userFound.providers)) {
                    let indexRemove = null;
                    for (let providerIndex in userFound.providers) {
                        if (userFound.providers.hasOwnProperty(providerIndex)) {
                            let provider = userFound.providers[providerIndex];
                            if (provider.driver == providerDriver) {
                                indexRemove = userFound.providers.indexOf(provider);
                                AuthProvider.findOne({_id: provider._id}).remove().exec();
                            }
                        }
                    }
                    if (indexRemove) {
                        userFound.providers.slice(indexRemove, 1);
                        return userFound.saveAsync();
                    }
                }
            })
    }

    getProviderByUserAndDriver(userId, driver) {
        return AuthProvider.findOne({user: userId, driver})
            .populate('user')
            .then(_handleEntityNotFound())
    }

    _createProvider(driver, token, refreshToken, expire, userId) {
        return AuthProvider.createAsync({
            driver,
            token,
            expire,
            refreshToken,
            user: userId
        }).then(()=> {
            return this.getProviderByUserAndDriver(userId, driver)
                .then((updatedProvider)=> {
                    updatedProvider.user.providers.push(updatedProvider._id);
                    return updatedProvider.user.saveAsync();
                })
        })
    }


}

const _handleEntityNotFound = function () {
    return function (entity) {
        if (!entity) {
            return null;
        }
        return entity;
    };
};

module.exports = MongoUserManager;
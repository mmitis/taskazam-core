const mongoose = require('mongoose');
const _ = require('lodash');
mongoose.Promise = require('bluebird');

const User = require('./../schemas/schema.User'),
    Filter = require('./../schemas/schema.Filter'),
    Task = require('./../schemas/schema.Task');

class MongoContentManager {

    constructor() {

    }

    storeItem(userId, item) {
        return new Promise((resolve, reject) => {
            let storeItem = item.getDbObject();
            storeItem.user = mongoose.Types.ObjectId(userId);
            Task.update(
                {_id: storeItem._id},
                storeItem,
                {upsert: true, setDefaultsOnInsert: true}, (err, inserted) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                    }
                    resolve(inserted);
                });
        });
    }

    storeAllItems(userId, itemArray) {
        return Promise.all(itemArray.map((item) => this.storeItem(userId, item)));
    }

    getItems(userId, filters) {
        return Task.find(_.merge({}, filters));
    }

    createFilter(user, name, params) {
        return Filter.createAsync({
            user,
            name,
            params

        });
    }

    getAllUserFilters(userId) {
        return Filter.find({user: userId});
    }

    getSomeUserFilters(userId, filtersId = []) {
        return Filter.find({
            user: userId, _id: {
                $in: filtersId.map((filterId) => mongoose.Types.ObjectId(filterId))
            }
        });
    }
}

const _handleEntityNotFound = function () {
    return function (entity) {
        if (!entity) {
            return null;
        }
        return entity;
    };
};

module.exports = MongoContentManager;